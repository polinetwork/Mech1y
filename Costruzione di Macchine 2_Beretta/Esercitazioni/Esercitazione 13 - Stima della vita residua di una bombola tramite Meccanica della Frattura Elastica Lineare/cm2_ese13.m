close all
clear all
clc

%%% Dati

D_es = 230; % mm
t = 5.8; % mm
p_es = 20; % MPa
R_m = 1000;
K_IC = 200; % MPa sqrt m

N = 12000; % numero cicli

m_paris = 3; % coefficiente di Paris
c_paris = 2.5e-13; % coefficiente di Paris
a0 = 0.05*t;
c0 = 5*a0;

p_statica = 2*p_es; 

F = 1; % fattore di forma

%%% Verifica statica

Sb = 0; % componente flettente
St = (p_statica * (D_es-2*t))/(2* t); % compoenente assiale sforzo calcolato con mariotte
w = 100; 

a = a0;
c = c0;
%K_statico = F*p_statica*sqrt(pi*c0);%sif
K_statico= SIF(t, w, a, c, 0, St, Sb)

if K_statico < K_IC*sqrt(1000)
    disp('Verifica statica OK')
else
    disp('Verifica statica FAILED')
end

%%% Calcolo a propagazione 

Sb = 0; % componente flettente
St = p_es*1.5/2/t*(D_es-2*t); % compoenente assiale
w = 100; 

a = a0;
c = c0;

K_a = SIF(t, w, a, c, pi/2, St, Sb);
K_c = SIF(t, w, a, c, 0, St, Sb);

dN = 10; % passo di integrazione
N=0;
while K_a < K_IC*sqrt(1000)&&  K_c < K_IC*sqrt(1000) && a < 0.95*t
    a = a + (c_paris * K_a^m_paris) * dN;
    c = c + (c_paris * K_c^m_paris) * dN;
    
    K_a = SIF(t, w, a, c, pi/2, St, Sb);
    K_c = SIF(t, w, a, c, 0, St, Sb);
    N=N+dN;
end

F = 1; % fattore di forma
K_I_propagazione = F * St * sqrt(pi * c)

if K_I_propagazione < K_IC*sqrt(1000)
    disp('Verifica a propagazione OK')
else
    disp('Verifica a propagazione FAILED')
end