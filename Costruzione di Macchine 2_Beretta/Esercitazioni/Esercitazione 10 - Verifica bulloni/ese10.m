clear all
close all 
clc

Eb=206000; Eg=69000; lb=164; 
D=400; Dintg=430; Dmg=440; d=24; R=Dmg/2;
b=10; h=10; e=25; t=80;
p=8;
n= D/40+4; n=16;
Agn=pi*Dmg*b/n;
Rp02b= 640; Rp02g=145; mu=0.333;
SIGbam=467; TAUbam=330;
eta1=2; 
SIGam=Rp02b/eta1;
dn1=sqrt(1.2*p*Dintg^2/n/SIGam);
dn=23.026; An=pi*dn^2/4;
%VALUTAZIONE CARICO SERRAGGIO
V0min=Rp02g*Agn;
V0max=Rp02b*An;
V0=V0min+2e3;
%VERIFICA TENUTA GUARNIZIONE
Kb=Eb*An/lb; Kg=Eg*Agn/h;
dlb=V0/Kb; dlg=V0/Kg;
P=p*pi*Dintg^2/4/n;
dl1=P/(Kb+Kg);
dPg=Kg*dl1; dPb=Kb*dl1;
Pg=V0-dPg; Pb=V0+dPb;
if Pg>2.5*p*Agn
    disp('Tenuta Guarnizione Verificata')
else
    disp('La guarnizione perde, PIRLA!')
end

SIGbax=Pb/An;

%COPPIA DI SERRAGGIO
Ms=0.2*V0*d;
Ms2=5*Ms/8;
TAUb=16*Ms2/pi/dn^3;

%EFFETTO COPPIE DISTRIBUITE DEI BULLONI
D=Eb*t^3/12/(1-mu^2);
m1=n*V0*e/pi/Dintg; alpha1=m1*R/(1+mu)/D;
m2=p*R^2/8; alpha2=m2*R/(1+mu)/D;
alpha=alpha1+alpha2;
SIGbf=alpha*Eb*dn/lb;

%VERIFICA RESISTENZA
SIG=SIGbf+SIGbax;
TAU=TAUb;

confronto=(SIG/SIGbam)^2+(TAU/TAUbam)^2;

if confronto>1
    disp('bulloni non verificati')
else
    disp('bulloni verificati')
end
