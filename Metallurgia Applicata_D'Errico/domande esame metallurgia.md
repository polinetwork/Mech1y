#Domande esame

### Il meccanismo di microsaldatura con asportazione progressiva degli strati superficiali è tipico di:

* **usura adesiva**;
* usura abrasiva;
* usura corrosiva.

### La presenza di cementite secondaria a bordo grano in uno strato cementato è indotto da:

* **potenziale di C > 1.2%**;
* potenziale di C < 0.8%;
* presenza di ossigeno in atmosfera cementante.

*Un'altra formulazione della domanda può essere: quando lo strato cementato non è buono? Il contenuto di C non deve essere mai oltre la concentrazione eutettoidica (0.8%), altrimenti il C in eccesso forma cementite secondaria (fragile) che porta a rotture dello strato cementato.*

### Per lavorazioni di macchine utensili ad alte velocità di taglio:

* acciai per utensili a caldo;
* **acciai rapidi**;
* acciai per utensili lavorazione a freddo.

*Gli acciai super rapidi e rapidi sono applicati proprio per alte velocità di taglio perchè resistono localmente anche per tempi prolungati. gli acciai per utensili a caldo si usano per lavorazioni a caldo per tempi molto prolungati.*

### Negli acciai HSLA (High Strength Low Alloy) l'incremento di caratteristiche meccaniche è imputabile a:

* **meccanismo di indurimento per precipitazione**;
* all'elevato carbonio presente;
* al meccanismo di indurimento per soluzione solida sostituionale.

*Sono a basso C;  essendo inoltre ridotta la presenza di elementi di lega che possono distorcere il reticolo, è da escludersi la soluzione solida sostituzionale.*

### Negli acciai al manganese (Hardfield) l'incremento di caratteristiche meccaniche è imputabile a:

* **fenomeno dell'incrudimento (auto bloccaggio dislocazioni)**;
* all'elevato C presente;
* meccanismo di indurimento per soluzione solida interstiziale.

*Non c'entra niente la presenza di C, mentre l'indurimento per soluzione solida non aumenta così tanto le caratteristiche meccaniche. La struttura di questo acciaio è austenitica: se lo deformo aumento tanto il carico a rottura (fino a 2000MPa), perché essendo CFC si generano molte dislocazioni che si bloccano a vicenda.*

### Gli acciai dual-phases consistono:

* **microstruttura mista ferrite-martensite**;
* microstruttura mista ferrite-austenite;
* microstruttura mista bainite-martensite.

*La terza è frutto di una tempra non andata a buon fine.*

### Nella classificazione delle ghise, il C equivalente è un parametro che tiene conto della modifica di struttura in dipendenza della presenza di:

* **C, Si e P**;
* C, Ni e P;
* C, Ca e P.

### La ghisa bianca presenta una struttura:

* con presenza di graffite lamellare;
* con presenza di graffite globulare;
* **in assenza di C in forma di graffite libera**.

*La differenza macro tra ghise bianche e grigie è la presenza di graffite libera. La ghisa bianca include però Fe3C sotto forma di ledebourite o ledebourite trasformata a seconda della temperatura.*

### A T ambiente una ghisa bianca ipo-eutettica manifesta struttura:

* perlitica;
* **perlitica con ledebourite trasformata**;
* austenitica.

### 34CrMoAl7 è un acciaio da:

* Cementazione;
* **Nitrurazione**;
* Bonifica.

*La presenza di Al (costoso e utilizzato solo per formare nitruri) ci fornisce la risposta. Inoltre la mancanza di Ni evita la formazione di uno strato superficiale fragile.*

### 34NiCrMo3 è un acciaio da:

* Cementazione;
* Nitrurazione;
* **Bonifica**.

*Per esclusione: Il Ni toglie la nitrurazione. C'è tanto C (sopra 0.20%) che esclude la cementazione.*

### Alla fine di un trattamento termico (tempra) di una lega di Alluminio, la sua durezza è:

* più alta della durezza a fine ciclo;
* più alta della durezza dopo invecchiamento;
* **più bassa della durezza iniziale**.

*Negli allumini dopo la tempra (detta tempra inversa), la durezza è più bassa di quella iniziale perchè si ha del Si disciolto nella matrice. A seguito dell'invechiamento, questo precipita in forma fine e blocca le dislocazioni.*

### Per impieghi criogenici è indicato:

* un acciaio 42CrMo4;
* **un acciaio X5CrNi1810;**
* un acciaio X5Cr18.

*Il Ni è quello che sposta verso basse temperature la temperatura di transizione duttile-fragile. Mettere tanto Ni si passa da CCC a CFC che è insensibile al fenomeno della transizione (curva piatta). Le leghe di Ti hanno anche loro la possibilità di essere utilizzate a basse temperature.*

####13NiCrMo4:

* **% Ni 1**;
* % Ni 4
* % Ni 5;

*in assenza di X all'inizio della sigla, prendo l'ultimo numero e lo divido per 4.*
