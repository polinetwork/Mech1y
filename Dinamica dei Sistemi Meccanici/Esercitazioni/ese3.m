clear all; close all; clc;

L=2; % m
S=1000;
rhoA=10;

x=0:0.001:L;

for kk=1:10
    gamma(kk)=kk*pi/L;
    omega(kk)=sqrt(S/rhoA)*gamma(kk);
    fi(:,kk)=sin(gamma(kk)*x);
end

%plot(x,fi(:,1))
%hold on
%plot(x,fi(:,2),'r')

% y=fi(:,9); % valore della deformata con il primo modo di vibrare
y(:,1)=sqrt(6-(x-1).^5); % deformata a semicirconferenza
%y=zeros(length(x),1);
%y(length(x)/2+0.5,1)=100;
yp=y*0; % velocit� nulla


for kk=1:10
    A(kk)=2/L*trapz(x,fi(:,kk).*y);
    B(kk)=2/L/omega(kk)*trapz(x,fi(:,kk).*yp);
end

% Tutte le B sono nulle perch� la velocit� inziale � nulla

fre=omega/2/pi;

t=0:0.01:1;

w(:,:)=zeros(length(t),length(x));

for tt=1:length(t)
    for kk=1:10
        w(tt,:)=w(tt,:)+(fi(:,kk)*(A(kk)*cos(omega(kk)*t(tt))+B(kk)*sin(omega(kk)*t(tt))))';
    end
end

% plot(x,w(1,:))
% hold on
% plot(x,w(10,:),'r')
% plot(x,w(15,:),'g')
% plot(x,w(20,:),'k')

figure
plot(x,w(1,:),'r')
axis([0 2 -1 1])
axis manual
    

for tt=1:length(t)
    plot(x,w(tt,:))
    axis([0 2 -1 1])
    pause(0.1)
end


