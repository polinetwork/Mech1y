clc
clear all
close all

%DATI
a_ref=1e-6; %m/s^2
a_lim_H=87; %dB
a_lim_V=89; %dB

%CARICA
[nomefile, cartella]=uigetfile('.mat','scegliere il file da caricare');

load([cartella,nomefile])

%VETTORE TEMPO
dt=1/fsamp;
t=0:dt:dt*length(data(:,1))-dt;

%CONVERTI IN EU
for ii=1:length(data(1,:))
    data(:,ii)=data(:,ii)/sens(ii);
end

%PLOT
figure
subplot(311)
plot(t,data(:,1),'LineWidth',2),grid on
ylabel('acc [m/s^2]')
title('asse x')
subplot(312)
plot(t,data(:,2),'LineWidth',2),grid on
ylabel('acc [m/s^2]')
title('asse y')
subplot(313)
plot(t,data(:,3),'LineWidth',2),grid on
ylabel('acc [m/s^2]')
xlabel('t [s]')
title('asse z')

%CALCOLO RMS SU TUTTO IL RECORD
devst=std(data);
media=mean(data);
rms_tot=sqrt(media.^2+devst.^2);
rms_tot_db=20*log10(rms_tot/a_ref);

disp('Limiti rms da normativa:')
disp(['Limite rms orizzontale: ' num2str(a_lim_H) ' dB']);
disp(['Limite rms verticale: ' num2str(a_lim_V) ' dB']);

disp(' ')
disp('Valori efficaci sull''intero record')
disp(['rms asse x: ' num2str(rms_tot_db(1)) ' dB']);
disp(['rms asse y: ' num2str(rms_tot_db(2)) ' dB']);
disp(['rms asse z: ' num2str(rms_tot_db(3)) ' dB']);

%CALCOLO I CREST FACTOR

for jj=1:3
    cf(jj)=max(abs(data(:,jj)-media(jj)))./rms_tot(jj);
end

% cf=max(abs(data))./rms_tot;

disp(' ')
disp('Crest factor')
disp(['cf asse x: ' num2str(cf(1))]);
disp(['cf asse y: ' num2str(cf(2))]);
disp(['cf asse z: ' num2str(cf(3))]);

pause

%CALCOLO DELL'RMS SU FINESTRA VIAGGIANTE DI 1s
rms=zeros(floor(length(data)/fsamp),3);
for ii=1:length(rms)
    rms(ii,:)=std(data((ii-1)*fsamp+1:(ii)*fsamp,:));
end
rms_db=20*log10(rms/a_ref);

%PLOT
figure
subplot(311)
plot(rms_db(:,1),'LineWidth',2),grid on,hold on
plot([0 length(rms_db(:,1))],[a_lim_H a_lim_H],'r')
ylabel('rms [dB]')
title('asse x')
subplot(312)
plot(rms_db(:,2),'LineWidth',2),grid on,hold on
plot([0 length(rms_db(:,2))],[a_lim_H a_lim_H],'r')
ylabel('rms [dB]')
title('asse y')
subplot(313)
plot(rms_db(:,3),'LineWidth',2),grid on,hold on
plot([0 length(rms_db(:,3))],[a_lim_V a_lim_V],'r')
ylabel('rms [dB]')
title('asse z')

%VERIFICA DEL RAPPORTO SEGNALE-RUMORE
figure
plot(data(:,3)),grid on
title('Seleziona una zona contenente solo rumore')
axis tight
pause
[x1 y1]=ginput(2);
title('Seleziona una zona contenente segnale e rumore')
pause
[x2 y2]=ginput(2);
s2n=20*log10(  std(data(floor(x2(1)):floor(x2(2)),:)) ./ std(data(floor(x1(1)):floor(x1(2)),:)));

disp(' ')
disp('Signal to noise ratio')
disp(['s2n asse x: ' num2str(s2n(1))]);
disp(['s2n asse y: ' num2str(s2n(2))]);
disp(['s2n asse z: ' num2str(s2n(3))]);
