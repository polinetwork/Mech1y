
close all
clc
clear all

[nomefile,cartella]=uigetfile('multiselect','on');

posizione_picco=[];
valore_picco=[];

%% Analisi di Fourier

for ttt=1:length(nomefile)

    load([cartella,char(nomefile(ttt))])
    % Segnale campionato
    N=length(y);
    t=[1/fs:1/fs:N*1/fs];
    y=y;
    % Finestro il segnale
    w=ones(N,1); %rettangolare
    %w=window(@hanning,N); %hanning
    %w=window(@flattopwin,N); %flat-top
    
    y=y.*w';
    
    figure(1)
    plot(t,y,'-*')
    xlabel('tempo [s]')
    ylabel('segnale')
   
    % fft
    trasf=fft(y);
    
    % Vettori delle frequenze
    df=1/t(end);
    freq=0:df:(N/2-1)*df;
    
    % Calco il modulo e normalizzo (prendo sempre N/2 dati)
    modulo=abs(trasf(1:N/2));
    modulo=modulo/(N/2);
    modulo(1)=modulo(1)/2;
    
    figure(2)
    stem(freq,modulo)
    xlabel('frequenza  [Hz]')
    ylabel('modulo')
    
    % Cerco il massimo del modulo
    [val,loc] = max(modulo);
    
    % Salvo il valore del massimo e la sua posizione in frequenza
    posizione_picco=[posizione_picco (loc-1)*df];
    valore_picco=[valore_picco val];
    
    pause
     
end

figure
plot(1:ttt,posizione_picco,'*')
title('Frequenza stimata')
xlabel('Campioni acquisiti')

figure
plot(1:ttt,valore_picco,'*')
title('Ampiezza stimata')
xlabel('Campioni acquisiti')




