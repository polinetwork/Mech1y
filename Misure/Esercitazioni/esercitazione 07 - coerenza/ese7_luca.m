clear all;
close all;
clc;

% Carico i dati
load('frullatore_aula@3483090965');
acc1 = Dati(:, 1);
mic1 = Dati(:, 2);

load('frullatore_aula@3483091087');
acc2 = Dati(:, 1);
mic2 = Dati(:, 2);

% Data la frequenza di campionamento e il deltaF voluto ricavo il numero di
% campioni da avere in un intervallo, nonch� quanti intervallini in totale
fsamp = 5120; % Hz
df_voluto = 2; % Hz
nc = fsamp/df_voluto;

numero_intervalli = length(acc1)/nc;

% Preparo vettori vuoti
fft_mic1 = zeros(nc, 1);
fft_acc1 = zeros(nc, 1);
fft_mic2 = zeros(nc, 1);
fft_acc2 = zeros(nc, 1);

gxx1 = zeros(nc/2, 1);
gxy1 = zeros(nc/2, 1);
gyx1 = zeros(nc/2, 1);
gyy1 = zeros(nc/2, 1);

gxx2 = zeros(nc/2, 1);
gxy2 = zeros(nc/2, 1);
gyx2 = zeros(nc/2, 1);
gyy2 = zeros(nc/2, 1);

% Ciclo sugli intervallini
for ii = 1:numero_intervalli
    % Indici di inizio e fine per prendere i dati solo dell'intervallino
    % richiesto
    inizio = nc*(ii - 1) + 1;
    fine = nc*(ii);
    
    % Calcolo FFT
    fft_mic1_loc = fft(mic1(inizio:fine));
    fft_acc1_loc = fft(acc1(inizio:fine));
    fft_mic2_loc = fft(mic2(inizio:fine));
    fft_acc2_loc = fft(acc2(inizio:fine));
    
    % Sommo tutti i componenti di Fourier per poi ottenere la media
    fft_mic1 = fft_mic1 + fft_mic1_loc;
    fft_acc1 = fft_acc1 + fft_acc1_loc;
    fft_mic2 = fft_mic2 + fft_mic2_loc;
    fft_acc2 = fft_acc2 + fft_acc2_loc;
    
    % Calcolo autospettri e compagnia sommandoli a quelli vecchi, per poi
    % mediare fuori dal for
    gxx1 = gxx1 + conj(fft_mic1_loc(1:nc/2)).*fft_mic1_loc(1:nc/2);
    gxy1 = gxy1 + conj(fft_mic1_loc(1:nc/2)).*fft_acc1_loc(1:nc/2);
    gyx1 = gyx1 + conj(fft_acc1_loc(1:nc/2)).*fft_mic1_loc(1:nc/2);
    gyy1 = gyy1 + conj(fft_acc1_loc(1:nc/2)).*fft_acc1_loc(1:nc/2);
    
    gxx2 = gxx2 + conj(fft_mic2_loc(1:nc/2)).*fft_mic2_loc(1:nc/2);
    gxy2 = gxy2 + conj(fft_mic2_loc(1:nc/2)).*fft_acc2_loc(1:nc/2);
    gyx2 = gyx2 + conj(fft_acc2_loc(1:nc/2)).*fft_mic2_loc(1:nc/2);
    gyy2 = gyy2 + conj(fft_acc2_loc(1:nc/2)).*fft_acc2_loc(1:nc/2);
end

% Faccio la media dei coefficienti di Fourier, prendendo solo la met� di
% interesse
fft_mic1 = fft_mic1(1:nc/2)/numero_intervalli;
fft_acc1 = fft_acc1(1:nc/2)/numero_intervalli;
fft_mic2 = fft_mic2(1:nc/2)/numero_intervalli;
fft_acc2 = fft_acc2(1:nc/2)/numero_intervalli;

% Divido per il totale per completare la media
gxx1 = gxx1/numero_intervalli;
gxy1 = gxy1/numero_intervalli;
gyx1 = gyx1/numero_intervalli;
gyy1 = gyy1/numero_intervalli;

gxx2 = gxx2/numero_intervalli;
gxy2 = gxy2/numero_intervalli;
gyx2 = gyx2/numero_intervalli;
gyy2 = gyy2/numero_intervalli;

% Asse frequenze
freq = 0:df_voluto:fsamp/2-df_voluto;

% Coerenza
figure(1);
coerenza1 = (abs(gxy1.*gyx1))./(gxx1.*gyy1);
plot(freq, coerenza1);
xlim([0 fsamp/2]);
ylim([0 1]);
xlabel('Frequenza [Hz]');
title('Coerenza set di dati 1');

figure(1);
coerenza2 = (abs(gxy2.*gyx2))./(gxx2.*gyy2);
plot(freq, coerenza2);
xlim([0 fsamp/2]);
ylim([0 1]);
xlabel('Frequenza [Hz]');
title('Coerenza set di dati 2');
