clear all;
close all;
clc;

[file, path] = uigetfile();
cd(path)
load(file)

%cd('/Users/luca/Documents/MATLAB/Misure/');
%load('generatore20Hz@3478082492.mat');

dimensione = size(Dati)

dt = 1/fsamp;

tempo = 0:dt:(dimensione(1)*dt)-dt;

% Grafico 1
dati1 = Dati(:, 1);
subplot(1, 2, 1)
plot(tempo, dati1)
grid on
hold all
xlabel('Tempo')
ylabel('Tensione')
title('Grafico 1')
max1 = max(dati1)
min1 = min(dati1)
mean1 = mean(dati1)
std1 = std(dati1)
rms1 = sqrt(mean1^2 + std1^2)
rms1_alternativo = sqrt((sum(dati1.^2))/length(dati1))
plot([0, tempo(end)], [max1, max1])
plot([0, tempo(end)], [min1, min1])
plot([0, tempo(end)], [mean1, mean1])
plot([0, tempo(end)], [std1, std1])
plot([0, tempo(end)], [rms1, rms1])
legend('Dato', 'Max', 'Min', 'Media', 'Dev. std', 'RMS');



% Grafico 2
dati2 = Dati(:, 2);
subplot(1, 2, 2)
plot(tempo, dati2)
grid on
hold all
xlabel('Tempo')
ylabel('Tensione')
title('Grafico 2')
max2 = max(dati2)
min2 = min(dati2)
mean2 = mean(dati2)
std2 = std(dati2)
rms2 = sqrt(mean2^2 + std2^2)
rms2_alternativo = sqrt((sum(dati2.^2))/length(dati2))
plot([0, tempo(end)], [max2, max2])
plot([0, tempo(end)], [min2, min2])
plot([0, tempo(end)], [mean2, mean2])
plot([0, tempo(end)], [std2, std2])
plot([0, tempo(end)], [rms2, rms2])
legend('Dato', 'Max', 'Min', 'Media', 'Dev. std', 'RMS');

save(strcat(file, '_risultati.mat'), 'max1', 'min1', 'mean1', 'std1', 'rms1', 'max2', 'min2', 'mean2', 'std2', 'rms2');