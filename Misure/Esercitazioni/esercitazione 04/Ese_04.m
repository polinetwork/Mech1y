clc
clear all
% close all

%CARICA I DATI
% load('009_ese_Dati.mat')

[nome cart]=uigetfile();
load([cart nome]);

Dati=Dati/sens*9.81;

%VETTORE TEMPO
dt=1/fsamp;
t=0:dt:dt*length(Dati)-dt;

%PLOT NEL TEMPO
figure
plot(t/60,Dati),grid on
title(nome)
xlabel('t [min]')
ylabel('m/s^2')

prompt={'risoluzione (df)','Colore'};
def={'0.01','b'};
risp=inputdlg(prompt,'Scegli i parametri',1,def);

df=str2double(char(risp(1)));


%SPETTRO E AUTOSPETTRO MEDIO
%-> imposto quanto sono lunghi i sottorecord
finestra=1/df;   % [s]


n_pti=round(finestra*fsamp);
passo=n_pti;

%-> alloco le variabili
sp_temp=zeros(n_pti,1);     %spettro del record ii-mo
sp_medio=zeros(n_pti,1);    %media degli spettri come vettori complessi
asp_medio=zeros(n_pti,1);   %media degli autospettri

%-> inizializzo le variabili
index_start=1;
index_end=n_pti;
count=0;

while index_end<=length(Dati)
    
    %-> estraggo il record ii-mo, lo finestro, calcolo la dft e la normalizzo
    sp_temp=fft(Dati(index_start:index_end))/n_pti;
    
    %-> sommo a sp_medio lo spettro ii-mo
    sp_medio=sp_medio+sp_temp;
    %-> sommo a asp_medio l'autospettro ii-mo
    asp_medio=asp_medio+conj(sp_temp).*sp_temp;
    
    %-> incremento le variabili
    count=count+1;
    index_start=index_start+passo;
    index_end=index_end+passo;
    
end

%-> divisio spettro e autospettro per il numero di record estratti
sp_medio=sp_medio/count;
asp_medio=asp_medio/count;

%-> estraggo le sole frequenze positive e moltiplico per due
sp_medio=sp_medio(1:fix(n_pti/2)+1);
asp_medio=asp_medio(1:fix(n_pti/2)+1);

sp_medio(2:end-1)=sp_medio(2:end-1)*2;
asp_medio(2:end-1)=asp_medio(2:end-1)*2;

%VETTORE FREQUENZA
T=n_pti*dt;
df=1/T;
freq=0:df:df*length(sp_medio)-df;

%PSD
psd_medio=asp_medio/df;

%PLOT
%spettro medio
figure (1)
semilogy(freq,abs(sp_medio),char(risp(2))),grid on
hold on
xlim([0.5 10])
xlabel('frequenza [Hz]')
ylabel('ampiezza [m/s^2]')
title('Media degli spettri')

%autospettro medio
figure (2)
semilogy(freq,asp_medio,char(risp(2))),grid on
hold on
xlim([0.5 10])
xlabel('frequenza [Hz]')
ylabel('ampiezza [(m/s^2)^2]')
title('Media degli autospettri')

%psd
figure(3)
semilogy(freq,psd_medio,char(risp(2))),grid on
hold on
xlim([0.5 10])
xlabel('frequenza [Hz]')
ylabel('ampiezza [(m/s^2)^2 / Hz]')
title('PSD media')